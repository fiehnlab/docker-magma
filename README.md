# MAGMa Dockerfile

This repository contains a Dockerfile of [MAGMa](https://github.com/NLeSC/MAGMa) for an automated build on [Docker Hub](https://hub.docker.com/r/ssmehta/magma/).  MAGMa (MS Annotation bsed on in-silico Generated Metabolites) is a tool for the automatic chemical annotation of accurate multistage MS<sup>n</sup> spectral data.

## Usage

To run a container in interactive mode

    docker run -it ssmehta/magma

This will run a container with the most recent revision from the MAGMa GitHub repository based on the latest [RDKit](https://hub.docker.com/r/ssmehta/rdkit/) build.  One additional version of the image containing commit `05f4798` of the MAGMa source based on RDKit Release_2016_03_1 is available and tagged as `05f4798`:

    docker run -it ssmehta/magma:05f4798

This specific build was used in Blaženović et al. (In Preparation) and is provided to allow for reproducibility of results.

It is possible to mount a host directory or attach data volume to the container's `/data` directory.  The entrypoint script is set up to look for and automatically execute a script at `/data/run.sh` it if it exists.  

## Building

Build with the usual

    docker build -t magma .

and then run with

    docker run -it magma
